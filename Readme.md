## Online Store Problem

### Problem Analysis
We have 2 issues
- Transaction is canceled when user has successfully checkout 

    the system does not have a queue or orchestrator regarding the availability of stock
- Misreported stock item 

    The system has no validation when the stock is 0 then it is not allowed for transactions

### Problem Solution

For issue transaction canceled

![](images/diagram.png)

from the diagram above, kita membutuhkan suatu service yang menghandle ketika ada order yang akan masuk (order service). 

order service will continue the workflow engine via kafka (event stream).

actually there are two solutions, full event driven or using workflows for orchestrate the microservice.

For systems that have a complicated flow, prefer to use a workflow engine, because it will make the micro service loosely decoupled.

in this POC, we orchestrate 2 service stock and transaction 

for issue misreported stock item 
- create validation, user can't purchase the item when empty stock  

### POC