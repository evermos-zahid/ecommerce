package main

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type Stock struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Quantity int    `json:"quantity"`
}

var Stocks = []Stock{}

func main() {
	r := gin.Default()

	stockRoutes := r.Group("/cart")
	{
		stockRoutes.GET("/", GetTransaction)
		stockRoutes.POST("/", CreateTransaction)
		stockRoutes.PUT("/:id", UpdateTransaction)
		stockRoutes.DELETE("/:id", DeleteTransaction)
	}

	if err := r.Run(":5000"); err != nil {
		log.Fatal(err.Error())
	}
}

func GetCart(c *gin.Context) {
	c.JSON(200, Stocks)
}

func CreateCart(c *gin.Context) {
	var reqBody Stock
	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.JSON(422, gin.H{
			"error":   true,
			"message": "invalid request body"})
		return
	}
	reqBody.ID = uuid.New().String()

	Stocks = append(Stocks, reqBody)

	c.JSON(200, gin.H{
		"error": false})
}

func UpdateCart(c *gin.Context) {
	id := c.Param("id")
	var reqBody Stock
	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.JSON(422, gin.H{
			"error":   true,
			"message": "invalid request body"})
		return
	}

	for i, u := range Stocks {
		fmt.Println("id ", u.ID)
		if u.ID == id {
			Stocks[i].Name = reqBody.Name
			Stocks[i].Quantity = reqBody.Quantity
		}
		c.JSON(200, gin.H{
			"error": false})
		return
	}

	c.JSON(404, gin.H{
		"error":   true,
		"message": "id not found"})
}

func DeleteCart(c *gin.Context) {
	id := c.Param("id")

	for i, u := range Stocks {
		fmt.Println("id ", u.ID)
		if u.ID == id {
			Stocks = append(Stocks[:i], Stocks[i+1:]...)
		}
		c.JSON(200, gin.H{
			"error": false})
		return
	}

	c.JSON(404, gin.H{
		"error":   true,
		"message": "id not found"})
}
